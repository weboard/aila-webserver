class Command {
    constructor(commandName, commandFunction) {
        this.name = commandName;
        this.func = commandFunction;
    }

    process(payload) {
        this.func(payload);
    }
}

let ailaManager = (function (){
    // Private members
    const APP_URL = process.env.VUE_APP_FS_URL;
    const APP_KEY = process.env.VUE_APP_FS_KEY;
    const SILENCE_TIMEOUT = process.env.VUE_APP_FSSILENCE_TIMEOUT;
    const DEVICE_ID = process.env.VUE_APP_FS_DEVICE_ID;
    const COMMAND_AUDIO = "audio";

    let listenBar;
    let UUID = createUUIDv4();

    let bDialogInitialized = false;
    let bInputEnabled = false;

    let audioArray = [];
    let voicesArray = [];
    let commandsArray = [];
    let currentCommandPosition = 0;

    let commands = [];
    let socket;

    // Private methods

    const messages = {
        initMessage: {
            "type": "Init",
            "key": APP_KEY,
            "appKey": APP_KEY,
            "deviceId": DEVICE_ID,
            "config": {
                "locale": "en",
                "zoneId": "Europe/Prague",
                "sttMode": "SingleUtterance",
                "sttSampleRate": 44100,
                "tts": "RequiredLinks",
                "ttsFileType": "wav",
                "returnSsml": false,
                "silenceTimeout": SILENCE_TIMEOUT,
                "test": false
            }
        },
        pokeMessage: {
            "type": "Request",
            "request": {
                "input": {
                    "transcript": {
                        "text": "#Intro",
                        "confidence": 1
                    }
                },
                "deviceId": DEVICE_ID,
                "appKey": APP_KEY,
                "sessionId": UUID,
                "attributes": {
                    "clientType": "UE4-WeBoard",
                    "clientScreen": true
                }
            }
        },
        inputAudioStreamOpen: {
            "type": "InputAudioStreamOpen",
            "appKey": APP_KEY
        },
        inputAudioStreamClose: {
            "type": "InputAudioStreamClose",
            "appKey": APP_KEY
        }
    };

    function createUUIDv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function resetSession() {
        UUID = createUUIDv4();
        messages.pokeMessage.request.sessionId = UUID;
        socket.send(JSON.stringify(messages.initMessage));
        emitUIInteraction("SessionStarted");
    }

    function setupVariables() {
        listenBar = document.getElementById("listenBar");
    }

    function setupSocket() {
        socket = new WebSocket(APP_URL);

        socket.addEventListener('open', function () {
            socket.send(JSON.stringify(messages.initMessage));
        });

        socket.addEventListener('message', function (event) {
            console.log('Message from server:', event.data);
            parseSocketMessage(JSON.parse(event.data));
        });
    }

    function setupAudio() {
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(function (mediaStream) {
                const context = new AudioContext();
                context.sampleRate = 44100;
                const gain = context.createGain();
                const audioSource = context.createMediaStreamSource(mediaStream);
                const analyser = context.createAnalyser();
                let bufferSize = 2048;

                const scriptNode = context.createScriptProcessor(bufferSize, 1, 1);
                audioSource.connect(gain);
                gain.connect(analyser);
                gain.connect(scriptNode);
                scriptNode.connect(context.destination);

                scriptNode.addEventListener("audioprocess", function(t) {
                    var n = t.inputBuffer.getChannelData(0),
                        data = Int16Array.from(n.map((function(t) {
                            var n = t < 0 ? 32768 * t : 32767 * t;
                            return Math.max(-32768, Math.min(32768, n))
                        })));

                    if (bInputEnabled) {
                        socket.send(data);
                    }
                });
            }, function (err) {
                console.log(err);
            });
    }

    function setupPixelStreamingListener() {
        // Events/Commands from PixelStreaming UE4 App
        addResponseEventListener("handle_responses", function(data) {
            console.log("New command from PixelStreaming received:" + data);
            switch (data) {
                case "StartInput":
                    socket.send(JSON.stringify(messages.inputAudioStreamOpen));
                    break;

                case "AudioFinished":
                    // Process every command until first Sound node
                    currentCommandPosition++;
                    while (currentCommandPosition < commandsArray.length && commandsArray[currentCommandPosition] != COMMAND_AUDIO) {
                        _processCommand(commandsArray[currentCommandPosition]);
                        currentCommandPosition++;
                    }
                    break;

                default:
                    console.log("This command is not implemented!");
            }
        });
    }

    function setupBasicCommands() {
        // Setup default Aila Voice
        commands.push(new Command("#AilaVoice", function(payload) {
            const descriptor = {
                AilaVoice: payload
            };
            emitUIInteraction(descriptor);
        }));

        // Setup default animation logic
        commands.push(new Command("#AilaAnim", function(payload) {
            emitUIInteraction(payload);
        }));

        // Setup default no-input logic
        commands.push(new Command("#Noinput", function () {
            bInputEnabled = false;
        }));

        // Setup default no-input logic
        commands.push(new Command("#SendResult", function () {
            const endScreen = document.getElementById("endScreen");
            endScreen.style.display = "block";
            setTimeout(function() {
                endScreen.classList.toggle("show");
            }, 100);
        }));
    }

    function parseSocketMessage(message) {
        switch (message.type) {
            case "Ready":
                console.log("Websocket is ready! Starting conversation...");
                socket.send(JSON.stringify(messages.pokeMessage));
                break;

            case "Response":
                console.log("Type: " + message.type);
                parseSocketResponse(message.response);
                break;

            case "InputAudioStreamOpen":
                console.log("Type: " + message.type);
                listenBar.classList.toggle('show');
                bInputEnabled = true;
                emitUIInteraction("attentionAnim");
                break;

            case "Recognized":
                console.log("Type: " + message.type);
                listenBar.classList.toggle('show');
                bInputEnabled = false;
                emitUIInteraction("noAttentionAnim");
                socket.send(JSON.stringify(messages.inputAudioStreamClose));
                break;

            case "SessionEnded":
                console.log("Type: " + message.type);
                emitUIInteraction("SessionEnded");
                break;

            case "SessionStarted":
                console.log("Type: " + message.type);
                emitUIInteraction("SessionStarted");
                break;


            default:
                console.log("Cannot parse socket message: " + message.type);
        }
    }

    function parseSocketResponse(response) {
        // Reset after each response from FS
        commandsArray = [];
        currentCommandPosition = 0;

        if (response.items && response.items.length) {
            for (let i = 0; i < response.items.length; i++) {
                // If we have audio property, send that to the UE4 App
                if (response.items[i].audio) {
                    audioArray.push(response.items[i].audio);
                    commandsArray.push(COMMAND_AUDIO);
                    if (response.items[i].ttsConfig && response.items[i].ttsConfig.name) {
                        voicesArray.push(response.items[i].ttsConfig.name);
                    } else {
                        voicesArray.push("custom");
                    }
                } else if (response.items[i].text) {
                    commandsArray.push(response.items[i]);
                }
            }

            // Process every command until first Audio node
            while (currentCommandPosition < commandsArray.length && commandsArray[currentCommandPosition] != COMMAND_AUDIO) {
                _processCommand(commandsArray[currentCommandPosition]);
                currentCommandPosition++;
            }

            sendAudio();
            console.log(commandsArray);
        }
    }

    function sendAudio() {
        let descriptor = {
            URLs: audioArray,
            Voices: voicesArray
        };

        emitUIInteraction(descriptor);
        audioArray = [];
        voicesArray = [];
    }

    function _processCommand(commandFromFS) {
        let bCommandFound = false;
        console.log(commandFromFS);
        commands.forEach(command => {
            if (command.name === commandFromFS.text) {
                command.process(commandFromFS.code);
                bCommandFound = true;
            }
        });

        if (bCommandFound) return;
        console.log(`Custom functionality for command ${commandFromFS.text} was NOT found. Sending the command straight to the UE4 App`);
        emitUIInteraction(commandFromFS.text);
    }

    // Public members
    return {
        // Adds new command to the commands array
        addCommand: function (commandName, commandFunction) {
            commands.push(new Command(commandName, commandFunction));
        },

        // Searches for command, if found -> run it's bound function
        processCommand: function(commandFromFS) {
            _processCommand(commandFromFS);
        },

        // Returns currently attached commands and their bound functions (#SendResults, ...)
        getCommands: function () {
            return commands;
        },

        // Initializes dialog through WebSocket with Flowstorm, starts session and conversation/messages handling
        initDialog: function () {
            // Can initialize only once
            if (bDialogInitialized) {
                console.log("Dialogue already initialized! Starting new session.");
                resetSession();
                return;
            }

            setupSocket();
            setupAudio();
            setupPixelStreamingListener();
            setupBasicCommands();
            setupVariables();

            bDialogInitialized = true;
        },

        initPixelStreaming: _initPixelStreaming,

        self: this
    }
})();

module.exports = ailaManager;

