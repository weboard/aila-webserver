// Requires
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const http = require('http');
const https = require('https');
const helmet = require('helmet');
const path = require('path');
const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;
const logging = require('./modules/core/logging.js');
const { exec } = require("child_process");

// Configuration setup
const defaultConfig = {
    "logToFile": true,
    "frontendPath": "/frontend/dist/",
    "homepageFile": "index.html",
    "httpPort": 80,
    "httpsPort": 443,
    "streamerPort": 8888,
    "enableAfkShutdown": false,
    "enableDisconnectShutdown:": false,
    "afkShutdownDuration": 600,
    "disconnectShutdownDuration": 600,
    "appPath": "C:/Users/Administrator/Desktop/PixelApp",
    "appName": "ProjectAila"
};

const argv = require('yargs').argv;
let configFile = (typeof argv.configFile != 'undefined') ? argv.configFile.toString() : path.join(__dirname, 'config.json');
const config = require("./modules/core/config.js").init(configFile, defaultConfig);

// Loging setup
logging.RegisterConsoleLogger();
if (config.logToFile) {
    logging.RegisterFileLogger('./logs');
}

const clientConfig = { type: 'config', peerConnectionOptions: {} };
console.log("Config: " + JSON.stringify(config, null, '\t'));
if (config.peerConnectionOptions) {
    clientConfig.peerConnectionOptions = JSON.parse(config.peerConnectionOptions);
}



// Server variables
const app = express();
const httpPort = config.httpPort;
const httpsPort = config.httpsPort;
const streamerPort = config.streamerPort;

// Certificate keys
const certKeys = {
    cert: fs.readFileSync('./certificates/cert1.pem'),
    key: fs.readFileSync('./certificates/privkey1.pem')
};

// Create HTTP/HTTPS Server
const serverHttp = http.createServer(app);
const serverHttps = https.createServer(certKeys, app);

// Run HTTP/HTTPS Servers
serverHttp.listen(httpPort, () => { console.logColor(logging.Green, `HTTP listening on port ${httpPort}`) });
serverHttps.listen(httpsPort, () => { console.logColor(logging.Green, `HTTPS listening on port ${httpsPort}`) });

// Middlewares
//app.use(helmet({ crossOriginResourcePolicy: { policy: "cross-origin" } }));
app.use(redirectToHTTPS());
app.use(express.static(path.join(__dirname, config.frontendPath)));

// Serve index.html as root route
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, config.frontendPath + config.homepageFile));
});

// Pixel Streamer WebSocket Connections
let WebSocket = require('ws');
let streamerServer = new WebSocket.Server({ port: streamerPort, backlog: 1 });
let streamer; // WebSocket connected to Streamer
let streamerConnected = false;

console.logColor(logging.Green, `WebSocket listening to Streamer connections on :${streamerPort}`)

streamerServer.on('connection', function (ws, req) {
    console.logColor(logging.Green, `Streamer connected: ${req.connection.remoteAddress}`);
    streamerConnected = true;

    ws.on('message', function onStreamerMessage(msg) {
        console.logColor(logging.Blue, `<- Streamer: ${msg}`);

        try {
            msg = JSON.parse(msg);
        } catch(err) {
            console.error(`cannot parse Streamer message: ${msg}\nError: ${err}`);
            streamer.close(1008, 'Cannot parse');
            return;
        }

        try {
            if (msg.type == 'ping') {
                streamer.send(JSON.stringify({ type: "pong", time: msg.time}));
                return;
            }

            let playerId = msg.playerId;
            delete msg.playerId; // no need to send it to the player

            // Convert incoming playerId to a string if it is an integer, if needed. (We support receiving it as an int or string).
            if(playerId && typeof playerId === 'number')
            {
                playerId = playerId.toString();
            }

            let player = players.get(playerId);
            if (!player) {
                console.log(`dropped message ${msg.type} as the player ${playerId} is not found`);
                return;
            }

            if (msg.type == 'answer') {
                player.ws.send(JSON.stringify(msg));
            } else if (msg.type == 'iceCandidate') {
                player.ws.send(JSON.stringify(msg));
            } else if (msg.type == 'disconnectPlayer') {
                player.ws.close(1011 /* internal error */, msg.reason);
            } else {
                console.error(`unsupported Streamer message type: ${msg.type}`);
                streamer.close(1008, 'Unsupported message type');
            }
        } catch(err) {
            console.error(`ERROR: ws.on message error: ${err.message}`);
        }
    });

    function onStreamerDisconnected() {
        disconnectAllPlayers();
        streamerConnected = false;
    }

    ws.on('close', function(code, reason) {
        try {
            console.error(`streamer disconnected: ${code} - ${reason}`);
            onStreamerDisconnected();
        } catch(err) {
            console.error(`ERROR: ws.on close error: ${err.message}`);
        }
    });

    ws.on('error', function(error) {
        try {
            console.error(`streamer connection error: ${error}`);
            ws.close(1006 /* abnormal closure */, error);
            onStreamerDisconnected();
        } catch(err) {
            console.error(`ERROR: ws.on error: ${err.message}`);
        }
    });

    streamer = ws;

    streamer.send(JSON.stringify(clientConfig));
});

let players = new Map(); // playerId <-> player, where player is either a web-browser or a native webrtc player
let nextPlayerId = 100;
let playerServer = new WebSocket.Server({ server: serverHttps});
console.logColor(logging.Green, `WebSocket listening to Players connections on :${httpsPort}`)

playerServer.on('connection', function (ws, req) {
    // Reject connection if streamer is not connected
    if (!streamer || streamer.readyState != 1 /* OPEN */) {
        ws.close(1013 /* Try again later */, 'Streamer is not connected');
        return;
    }

    let playerId = (++nextPlayerId).toString();
    console.log(`player ${playerId} (${req.connection.remoteAddress}) connected`);
    players.set(playerId, { ws: ws, id: playerId });

    function sendPlayersCount() {
        let playerCountMsg = JSON.stringify({ type: 'playerCount', count: players.size });
        for (let p of players.values()) {
            p.ws.send(playerCountMsg);
        }
    }

    ws.on('message', function (msg) {
        console.logColor(logging.Blue, `<- player ${playerId}: ${msg}`);

        try {
            msg = JSON.parse(msg);
        } catch (err) {
            console.error(`Cannot parse player ${playerId} message: ${err}`);
            ws.close(1008, 'Cannot parse');
            return;
        }

        if (msg.type == 'offer') {
            console.log(`<- player ${playerId}: offer`);
            msg.playerId = playerId;
            streamer.send(JSON.stringify(msg));
        } else if (msg.type == 'iceCandidate') {
            console.log(`<- player ${playerId}: iceCandidate`);
            msg.playerId = playerId;
            streamer.send(JSON.stringify(msg));
        } else if (msg.type == 'stats') {
            console.log(`<- player ${playerId}: stats\n${msg.data}`);
        } else if (msg.type == 'kick') {
            let playersCopy = new Map(players);
            for (let p of playersCopy.values()) {
                if (p.id != playerId) {
                    console.log(`kicking player ${p.id}`)
                    p.ws.close(4000, 'kicked');
                }
            }
        } else {
            console.error(`<- player ${playerId}: unsupported message type: ${msg.type}`);
            ws.close(1008, 'Unsupported message type');
            return;
        }
    });

    function onPlayerDisconnected() {
        try {
            players.delete(playerId);
            streamer.send(JSON.stringify({type: 'playerDisconnected', playerId: playerId}));
            sendPlayersCount();
        } catch(err) {
            console.logColor(logging.Red, `ERROR:: onPlayerDisconnected error: ${err.message}`);
        }

        // Check whether after {disconnectShutdownDuration} there is still any player connected. If not, shutdown the server
        if (!config.enableDisconnectShutdown) return;
        setTimeout(function() {
            if (players.size == 0) {
                exec("shutdown /s", (error, stdout, stderr) => {});
            }
        }, config.disconnectShutdownDuration * 1000);
    }

    ws.on('close', function(code, reason) {
        console.logColor(logging.Yellow, `player ${playerId} connection closed: ${code} - ${reason}`);
        onPlayerDisconnected();
    });

    ws.on('error', function(error) {
        console.error(`player ${playerId} connection error: ${error}`);
        ws.close(1006 /* abnormal closure */, error);
        onPlayerDisconnected();

        console.logColor(logging.Red, `Trying to reconnect...`);
        reconnect();
    });

    ws.send(JSON.stringify(clientConfig));

    sendPlayersCount();
});

function disconnectAllPlayers(code, reason) {
    let clone = new Map(players);
    for (let player of clone.values()) {
        player.ws.close(code, reason);
    }
}

// Shutdown instance/server after certain amout of time running the server
if (config.enableAfkShutdown) {
    setTimeout(function () {
        exec("shutdown /s", (error, stdout, stderr) => {});
    }, config.afkShutdownDuration * 1000);
}

////////////////////
// Custom endpoints
////////////////////

// Start the app on AWS instance
app.get('/startapp', function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    exec(`start cmd.exe /k "${config.appPath}/StartPixelApp.bat"`, (error, stdout, stderr) => {});
    res.send("App Started");
});

// Restart the app on AWS instance
app.get('/restartapp', function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    exec(`taskkill /f /im "${config.appName}.exe" /t`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            res.send(error.message);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            res.send(stderr);
            return;
        }
        console.log(`stdout: ${stdout}`);

        res.send("App Restarted");
    });
});

// Check whether the streamer app is running and connected to the WebServer
app.get('/streamer', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.send(streamerConnected);
});


////////////////////////
// Client functionality
////////////////////////
let client = require("./modules/client/client.js");